
Agile Lab needs to adapt quickly to market changes from both an organizational and an operational point of view, so we want to define a lean, result-oriented organization where everyone's roles and accountabilities are well defined. The decision-making autonomy of Agile Lab staff is key to speeding up the process and is based on the assumption that all the people are highly qualified to make decisions on their own.

Within Agile Lab there are several "roles" and "workgroups" that we will name "circles" that are associated with a set of goals, domains and responsibilities, within this perimeter the people who are assigned to these roles and workgroups are autonomous with delegation power.

The definition of roles and circles and their attributes are defined by this handbook, which is public and is updated periodically to tailor the organization to the needs of the business.
The handbook aims to make business rules and functions explicit and set expectations for each role or circle. This organizational clarity is tasked with empowering the people in the various roles and making them autonomous in making decisions within a perimeter where they feel safe and protected (by the handbook itself).

Within a circle there may be others sub-circles and roles, also each circle has a leader who is in charge of organizing the circle itself independently.
The leader of each circle has the task of reporting during the governance meetings the issues of this, that are not addressable (according to the handbook) independently, to the upper circle so that they can be addressed in a structural and organizational.

# Circles

Here the list of Circles in a hierarchical view

* General Company
    *   Board
    *   HR
    *   Finance
    *   Sales
    *   Engineering
        *   Project & Product delivery
            * Project 1
            * Project 2
            * ...
        *   Infrastructure and Support
        *   Aim2
        *   Software Development
    *   AgileSkill - training
    *   Marketing
    

# Who does what ?

In Agile Lab we talk about roles, we don't talk about people. Each person can be assigned to multiple roles and each role can be covered by multiple people.
A role has well defined accountabilities and purpose. Roles will be created and destroyed based on what is currently needed by the company to obtain best results, generating extreme flexibility in the organization.

Role List:

*   CEO
*   CTO
*   COO
*   CFO
*   Head of Software Development
*   Software Mentor
*   Project Leader
*   Big Data Engineer
*   Data Scientist
*   Head of Architectures
*   Architect
*   Thesis Tutor
*   Buddy
*   Pre Sales Architect
*   Head of Support and Infrastructure
*   Head of Aim2
*   DevOps
*   SysOps
*   Head of Agile Skill
*   Trainer
*   PMO
*   Account
*   Head of Sales
*   Head of Marketing
*   Business Developer
*   Sales
*   Business Partner
*   Finance Controller
*   Head of HR
*   Talent Scout
*   Interviewer
*   Head of Marketing
*   Social Manager
*   SEO Expert
*   Graphic Designer
*   Web Designer
*   Tech Writer
*   Event Specialist
*   Facility Manager





